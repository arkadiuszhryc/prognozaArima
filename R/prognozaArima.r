prognozaArima <- function(ticker) {
  library(forecast)
  tolower(ticker)
  url <- paste("https://stooq.pl/q/d/l/?s=", ticker, "&i=d", sep = "")
  download.file(url, destfile = "notowania.csv")
  instrumentCena <- read.table(file = "notowania.csv", sep = ",", header = TRUE, blank.lines.skip = TRUE, fill = TRUE)
  instrumentCena <- instrumentCena[, 5]
  file.remove("notowania.csv")
  dl <- length(instrumentCena)
  instrumentCenaNauka <- instrumentCena[1:(dl/2)]
  arimaCena <- auto.arima(instrumentCenaNauka)
  if (grepl("drift", arimaCena)) {
    dryf <- TRUE
  } else {
    dryf <- FALSE
  }
  model <- Arima(instrumentCenaNauka, order = arimaorder(arimaCena), include.drift = dryf)
  prognoza <- forecast(model, h = dl/2)
  tytul <- paste(arimaorder(arimaCena), collapse=", ")
  tytul <- paste("ARIMA(", tytul, ")", sep = "")
  if (dryf == TRUE) {
    tytul <- paste(tytul, "z dryfem", sep = " ")
  }
  tytul <- paste(toupper(ticker), tytul, sep = " - ")
  plot(prognoza, main = tytul, xlab = "Dzien notowan", ylab = "Cena")
  lines(instrumentCena)
}