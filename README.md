prognozaArima - pakiet języka R
===============================

Instalacja
----------

```
install.packages("devtools")
devtools::install_git("https://gitlab.com/arkadiuszhryc/prognozaArima.git")
```

Używanie
--------

1. `library(prognozaArima)` - wczytanie paczki

2. `prognozaArima("ticker")`, gdzie ticker, to ticker z serwisu stooq.pl, np. "wig"
dla indeksu WIG.

Licencja
--------

Pakiet dostępny na licencji MIT.